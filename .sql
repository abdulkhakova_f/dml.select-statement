1. Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?

Solution 1:


SELECT
    s.store_id,
    s.manager_staff_id,
    CONCAT(st.first_name, ' ', st.last_name) AS manager_name,
    SUM(p.amount) AS total_revenue
FROM
    payment p
INNER JOIN
    staff st ON p.staff_id = st.staff_id
INNER JOIN
    store s ON st.store_id = s.store_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id,
    s.manager_staff_id,
    manager_name
HAVING
    SUM(p.amount) = (
        SELECT
            MAX(total_revenue)
        FROM (
            SELECT
                s.store_id,
                SUM(p.amount) AS total_revenue
            FROM
                payment p
            INNER JOIN
                staff st ON p.staff_id = st.staff_id
            INNER JOIN
                store s ON st.store_id = s.store_id
            WHERE
                EXTRACT(YEAR FROM p.payment_date) = 2017
            GROUP BY
                s.store_id
        ) AS max_revenue
        WHERE
            max_revenue.store_id = s.store_id
    );


========================================================================================

WITH StaffRevenue AS (
    SELECT
        s.store_id,
        s.manager_staff_id,
        CONCAT(st.first_name, ' ', st.last_name) AS manager_name,
        SUM(p.amount) AS total_revenue,
        ROW_NUMBER() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS rank
    FROM
        payment p
    INNER JOIN
        staff st ON p.staff_id = st.staff_id
    INNER JOIN
        store s ON st.store_id = s.store_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id,
        s.manager_staff_id,
        manager_name
)
SELECT
    store_id,
    manager_staff_id,
    manager_name,
    total_revenue
FROM
    StaffRevenue
WHERE
    rank = 1;
===============================================================================================================

2.Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

Solution;1

WITH RankedFilms AS (
    SELECT
        f.title AS film_title,
        COUNT(*) AS rental_count,
        f.rating as rating,
        CASE f.rating
            WHEN 'G' THEN 'Suitable for all ages'
            WHEN 'PG' THEN 'around 7 years and older'
            WHEN 'PG-13' THEN 'around 13 years and older'
            WHEN 'R' THEN '17 or 18 and older'
            WHEN 'NC-17' THEN '18 and older'
            END AS expected_age,
        ROW_NUMBER() OVER (ORDER BY COUNT(*) DESC) AS rn
    FROM film f
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    GROUP BY f.film_id, f.title, f.rating
)
SELECT film_title, rental_count, rating, expected_age
FROM RankedFilms
WHERE rn <= 5
ORDER BY rental_count DESC;

================================================================================================================================

WITH TopRentedMovies AS (
    SELECT
        f.film_id,
        f.title AS film_title,
        COUNT(r.rental_id) AS rental_count,
        ROW_NUMBER() OVER (ORDER BY COUNT(r.rental_id) DESC) AS rank
    FROM
        rental r
    INNER JOIN
        inventory i ON r.inventory_id = i.inventory_id
    INNER JOIN
        film f ON i.film_id = f.film_id
    GROUP BY
        f.film_id,
        f.title
)
SELECT
    trm.film_id,
    trm.film_title,
    trm.rental_count,
    AVG(EXTRACT(YEAR FROM CURRENT_DATE) - EXTRACT(YEAR FROM c.birth_date)) AS expected_age
FROM
    TopRentedMovies trm
JOIN
    rental r ON trm.film_id = r.film_id
JOIN
    customer c ON r.customer_id = c.customer_id
WHERE
    trm.rank <= 5
GROUP BY
    trm.film_id,
    trm.film_title,
    trm.rental_count
ORDER BY
    trm.rental_count DESC;

==========================================================================================================================================================================================




SELECT
    actor_id,
    first_name,
    last_name,
    MAX(EXTRACT(YEAR FROM CURRENT_DATE)) - MAX(EXTRACT(YEAR FROM film.last_update)) AS years_since_last_film
FROM
    actor
LEFT JOIN
    film_actor ON actor.actor_id = film_actor.actor_id
LEFT JOIN
    film ON film_actor.film_id = film.film_id
GROUP BY
    actor_id,
    first_name,
    last_name
ORDER BY
    years_since_last_film DESC;
======================================================================================================================================
3.

Solition 1

SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(EXTRACT(YEAR FROM CURRENT_DATE)) - MAX(EXTRACT(YEAR FROM f.last_update)) AS years_since_last_film
FROM
    actor a
LEFT JOIN
    film_actor fa ON a.actor_id = fa.actor_id
LEFT JOIN
    film f ON fa.film_id = f.film_id
GROUP BY
    a.actor_id,
    a.first_name,
    a.last_name
ORDER BY
    years_since_last_film DESC;

==========================================================================================================================
SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    EXTRACT(YEAR FROM CURRENT_DATE) - COALESCE(MAX(EXTRACT(YEAR FROM f.last_update)), 0) AS years_since_last_film
FROM
    actor a
LEFT JOIN
    film_actor fa ON a.actor_id = fa.actor_id
LEFT JOIN
    film f ON fa.film_id = f.film_id
GROUP BY
    a.actor_id,
    a.first_name,
    a.last_name
ORDER BY
    years_since_last_film DESC;

